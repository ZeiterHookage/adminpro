import { Injectable, NgZone } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {registerForm} from '../interfaces/register-form.interface';
import {LoginForm} from '../interfaces/login-form.interface';
import { environment } from 'src/environments/environment';
import {tap,map,catchError, delay}from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import {Usuario} from '../models/usuario.model';
import {CargarUsuario} from '../interfaces/cargar-usuarios.interface';

const base_url=environment.base_url;
declare const gapi;
@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
 public  auth2:any;
 public usuario:Usuario;
  constructor(private http:HttpClient,private router:Router,private ngZone:NgZone) {
    this.googleInit();
  }

  googleInit(){
    return new Promise<void>(resolve=>{
      gapi.load('auth2', ()=>{
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      this.auth2 = gapi.auth2.init({
        client_id: '32826377538-3dvv0rdq7rr6eh5bsrr04jd53e96b904.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',

      });
      resolve();
    })

    });
  }
  get token():string{
    return localStorage.getItem('token') || '';
  }
  get uid():string{
    return this.usuario.uid || '';
  }
  get headers(){
   return { headers:{
      'x-token':this.token
    }}
  }
  logout(){
    localStorage.removeItem('token');

    this.auth2.signOut().then(()=>{
      this.ngZone.run(()=>{
        this.router.navigateByUrl('/login');
      })

    })
  }
  validarToken():Observable<boolean>{

    return this.http.get(`${base_url}/login/renew`,{
      headers:{
        'x-token':this.token
      }
    }).pipe(
      tap((resp:any)=>{
      const{email,google,img,nombre,role,uid}=resp.usuario;
        this.usuario=new Usuario(nombre,email,'',role,google,img,uid);

        localStorage.setItem('token',resp.token);
      }),
      map(resp=>true),
      catchError(err=>of(false))

    )
  }
  crearUsuario(formData:registerForm){
return this.http.post(`${base_url}/usuarios`,formData).pipe(tap((resp:any)=>{
  localStorage.setItem('token',resp.token);
}));
  }
  actualizarPerfil(data:{email:string,nombre:string,role:string}){
    data={
      ...data,
      role:this.usuario.role

    }
    return this.http.put(`${base_url}/usuarios/${this.uid}`,data,this.headers)

  }
  login(formData:LoginForm){
    return this.http.post(`${base_url}/login`,formData).pipe(tap((resp:any)=>{
      localStorage.setItem('token',resp.token);
    }));
      }
      loginGoogle(token){
        return this.http.post(`${base_url}/login/google`,{token}).pipe(tap((resp:any)=>{
          localStorage.setItem('token',resp.token);
        }));
          }
cargarUsuarios(desde:number=0){
  const url=`${base_url}/usuarios?desde=${desde}`
return this.http.get<CargarUsuario>(url, this.headers)
.pipe(
  delay(500),
  map(resp=>{
  const usuarios= resp.usuarios.map(user=>new Usuario(user.nombre,user.email,'',user.role,user.google,user.img,user.uid))
  return {
    total: resp.total,
   usuarios
  };
}));
}
eliminarUsuario(usuario:Usuario){
  const url=`${base_url}/usuarios/${usuario.uid}`
  return this.http.delete(url, this.headers)
}
guardarUsuario(usuario:Usuario){
  /*data={
    ...data,
    role:this.usuario.role

  }*/
  return this.http.put(`${base_url}/usuarios/${usuario.uid}`,usuario,this.headers)

}
}
